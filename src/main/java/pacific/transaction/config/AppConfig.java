package pacific.transaction.config;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import pacific.transaction.repository.ItemRepository;
import pacific.transaction.service.ItemService;
import pacific.transaction.service.ItemServiceImpl;

@Configuration
@ComponentScan({"pacific.transaction"})
public class AppConfig {

    @Bean(name = "itemService")
    @Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
    public ItemService getItemService() {
        ItemServiceImpl itemService = new ItemServiceImpl(getItemRepository());
        //itemService.setItemRepository(getItemRepository());
        return itemService;
    }

    @Bean(name = "itemRepository")
    public ItemRepository getItemRepository() {
        return new ItemRepository();
    }
}
