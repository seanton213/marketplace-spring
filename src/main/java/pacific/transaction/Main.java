package pacific.transaction;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pacific.transaction.config.AppConfig;
import pacific.transaction.service.ItemService;

public class Main {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);

        ItemService itemService = ctx.getBean("itemService", ItemService.class);
    }
}
